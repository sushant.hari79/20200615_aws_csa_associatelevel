Agenda :

1. Route53 

2. IAM User Creation & Configure User from AWS CLI 
    - Launch an EC2 instance of Windows and Linux using CLI 

https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/routing-policy.html#routing-policy-weighted
https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/routing-policy.html

https://aws.amazon.com/route53/

https://github.com/keshavkummari/codewithckk

https://www.iana.org/



Use Case :

- Host a Customer on AWS :
    
    Name of the Customer : code with ckk Inc 

    Have you purchased a Domain Name for your business : Yes
        - What is the name of the Domain : codewithckk.com 
        - From Where they bought : godaddy.com 

Host a Website / WebApplication on Cloud : AWS 
    - Website code :
        - Java 
        - NodeJS
        - Python
        - HTML, CSS & JS 
code.zip / .jar / .ear / .tar.gz      

Build infrastructure on AWS and deploy the code 

Business Domain : Online Education 

Language in which application is written : HTML, CSS & JS 

Type of Tier : 

1 Tier :

Static WebApplication : 1 Tier 

Public Subnets : Administration Layer 

Private Subnets : Web Layer 


2 Tier (Web Layer & Database Layer)

3 Tier Architecture :

Web Layer 
App Layer 
Database Layer 